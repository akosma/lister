use chrono::{Local, NaiveDate, NaiveDateTime};
use clap::Parser;
use itertools::Itertools;
use std::fs;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

/// Structure used to hold all arguments and parameters passed to the application.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// (Mandatory) Path of the posts to parse
    path: String,

    /// (Optional) Order results from the younger to older
    #[arg(short, long)]
    invert: bool,

    /// (Optional) Display date next to filename
    #[arg(short, long)]
    verbose: bool,

    /// (Optional) Used to start filtering; format is 2024-05-07T14:36:00
    #[arg(short, long)]
    start: Option<NaiveDateTime>,

    /// (Optional) Used to end filtering; format is 2024-05-07T14:36:00
    #[arg(short, long)]
    end: Option<NaiveDateTime>,

    /// (Optional) Used to list all entries of a particular year; if provided, it ignores "start" and "end" arguments
    #[arg(short, long)]
    year: Option<i32>,
}

/// Structure used to hold information of a particular Markdown file
/// including its full filename and its publication date, as specified
/// within its contents.
#[derive(PartialEq, Debug)]
struct MarkdownFile {
    filename: String,
    publication_datetime: NaiveDateTime,
}

/// Does what the name says.
fn list_files(path: &str) -> fs::ReadDir {
    fs::read_dir(path).unwrap()
}

/// Takes a path result and returns the actual filename to parse.
fn get_filename(path: Result<fs::DirEntry, io::Error>) -> String {
    format!("{}/index.md", path.unwrap().path().display())
}

/// Parses the filename passed as parameter and returns an optional MarkdownFile instance
fn parse(filename: String) -> Option<MarkdownFile> {
    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }

    let lines = read_lines(filename.clone()).ok()?;
    for line in lines.flatten() {
        if let Some(key) = line.strip_prefix("date: ") {
            // Attempt to parse as a NaiveDate
            if let Ok(publication_date) = NaiveDate::parse_from_str(key, "%Y-%m-%d") {
                // Convert NaiveDate to NaiveDateTime
                let publication_datetime = publication_date
                    .and_hms_opt(12, 0, 0)
                    .unwrap_or_else(|| NaiveDateTime::from_timestamp(0, 0));
                return Some(MarkdownFile {
                    filename,
                    publication_datetime,
                });
            }
            // If first parsing fails, attempt to parse with NaiveDateTime
            if let Ok(publication_datetime) =
                NaiveDateTime::parse_from_str(key.trim_matches('\''), "%Y-%m-%dT%H:%M:%S%z")
            {
                return Some(MarkdownFile {
                    filename,
                    publication_datetime,
                });
            }
            return None; // Parsing failed for both formats
        }
    }
    None
}

/// Returns a tuple with a range that covers a full year
fn get_year_range(year: Option<i32>) -> (Option<NaiveDateTime>, Option<NaiveDateTime>) {
    if year.is_none() {
        return (None, None);
    }
    // Try to create January 1st at 00:00
    let start_of_year =
        NaiveDate::from_ymd_opt(year.unwrap(), 1, 1).and_then(|date| date.and_hms_opt(0, 0, 0));

    // Try to create December 31st at 23:59
    let end_of_year = NaiveDate::from_ymd_opt(year.unwrap(), 12, 31)
        .and_then(|date| date.and_hms_opt(23, 59, 59));

    (start_of_year, end_of_year)
}

/// If start is not provided, it returns the Unix Epoch. If end is not provided, it returns today's date.
fn set_default_dates(
    start: Option<NaiveDateTime>,
    end: Option<NaiveDateTime>,
) -> (NaiveDateTime, NaiveDateTime) {
    // Default for `start` is the Unix Epoch
    let start = start.unwrap_or_else(|| NaiveDateTime::from_timestamp(0, 0));
    // Default for `end` is the current date and time (today)
    let end = end.unwrap_or_else(|| Local::now().naive_local());

    (start, end)
}

/// Returns a closure encapsulating the date of today
fn make_date_filter(
    start: Option<NaiveDateTime>,
    end: Option<NaiveDateTime>,
) -> Box<dyn Fn(MarkdownFile) -> Option<MarkdownFile>> {
    // Required for the calculation of dates
    let (s, e) = set_default_dates(start, end);
    Box::new(move |f| {
        let in_range = f.publication_datetime >= s && f.publication_datetime <= e;
        in_range.then(|| f)
    })
}

/// Simply print the MarkdownFile item on the console
fn print(file: MarkdownFile, verbose: bool) {
    if verbose {
        println!("{}: {}", file.publication_datetime, file.filename);
    } else {
        println!("{}", file.filename);
    }
}

/// Get list of all post subfolders, get filenames, parse, validate, sort, and print
fn process(
    path: &String,
    start: Option<NaiveDateTime>,
    end: Option<NaiveDateTime>,
    invert: bool,
) -> Vec<MarkdownFile> {
    let result = list_files(&path)
        .map(get_filename)
        .filter_map(parse)
        .filter_map(make_date_filter(start, end))
        .sorted_by(|a, b| a.publication_datetime.cmp(&b.publication_datetime));
    if invert {
        result.rev().collect()
    } else {
        result.collect()
    }
}

/// Main entry point
fn main() {
    // Verify that we got a proper argument to the location of posts
    let args = Args::parse();
    // The "year" argument takes precedence over the "start" and "end" arguments
    let (start, end) = if let Some(year) = args.year {
        get_year_range(Some(year))
    } else {
        (args.start, args.end)
    };
    process(&args.path, start, end, args.invert)
        .into_iter()
        .for_each(|item| print(item, args.verbose));
}

/// Test suite
#[cfg(test)]
fn generate_fixtures_helper() -> (MarkdownFile, MarkdownFile, MarkdownFile) {
    let first = MarkdownFile {
        filename: String::from("fixtures/first/index.md"),
        publication_datetime: NaiveDateTime::parse_from_str(
            "2019-04-21T12:00:00+02:00",
            "%Y-%m-%dT%H:%M:%S%z",
        )
        .unwrap(),
    };
    let second = MarkdownFile {
        filename: String::from("fixtures/second/index.md"),
        publication_datetime: NaiveDateTime::parse_from_str(
            "2021-05-24T12:00:00+02:00",
            "%Y-%m-%dT%H:%M:%S%z",
        )
        .unwrap(),
    };
    let third = MarkdownFile {
        filename: String::from("fixtures/third/index.md"),
        publication_datetime: NaiveDateTime::parse_from_str(
            "2022-05-24T01:05:03+02:00",
            "%Y-%m-%dT%H:%M:%S%z",
        )
        .unwrap(),
    };
    (first, second, third)
}

#[test]
fn test_normal_operation() {
    let path = String::from("fixtures");
    let mut result = process(&path, None, None, false).into_iter();
    let (first, second, third) = generate_fixtures_helper();
    // The "fourth/index.md" and "fifth/index.md" files must not appear, because they are scheduled far away in the future
    assert_eq!(Some(first), result.next());
    assert_eq!(Some(second), result.next());
    assert_eq!(Some(third), result.next());
    assert_eq!(None, result.next());
    assert_eq!(None, result.next());
}

#[test]
fn test_inverse_result() {
    let path = String::from("fixtures");
    let mut result = process(&path, None, None, true).into_iter();
    let (first, second, third) = generate_fixtures_helper();
    // Same results as test "test_normal_operation" but with items in inverse order
    assert_eq!(Some(third), result.next());
    assert_eq!(Some(second), result.next());
    assert_eq!(Some(first), result.next());
    assert_eq!(None, result.next());
    assert_eq!(None, result.next());
}

#[test]
fn test_date_filter() {
    let path = String::from("fixtures");
    let start = NaiveDateTime::parse_from_str("2018-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S").unwrap();
    let end = NaiveDateTime::parse_from_str("2020-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S").unwrap();
    let mut result = process(&path, Some(start), Some(end), true).into_iter();
    let (first, _, _) = generate_fixtures_helper();
    // Only the first item has a date between the limits specified
    assert_eq!(Some(first), result.next());
    assert_eq!(None, result.next());
    assert_eq!(None, result.next());
}

#[test]
fn test_wrong_date_filter() {
    let path = String::from("fixtures");
    let end = NaiveDateTime::parse_from_str("2018-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S").unwrap();
    let start = NaiveDateTime::parse_from_str("2020-01-01T00:00:00", "%Y-%m-%dT%H:%M:%S").unwrap();
    let mut result = process(&path, Some(start), Some(end), true).into_iter();
    // The start and end dates are inverted, so no results are shown
    assert_eq!(None, result.next());
}

#[test]
fn test_year_filter() {
    let path = String::from("fixtures");
    let (start, end) = get_year_range(Some(2019));
    let mut result = process(&path, start, end, true).into_iter();
    let (first, _, _) = generate_fixtures_helper();
    // Only the first item has a date between the limits specified
    assert_eq!(Some(first), result.next());
    assert_eq!(None, result.next());
    assert_eq!(None, result.next());
}
