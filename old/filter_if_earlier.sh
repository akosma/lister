#!/usr/bin/bash

# This script only outputs if the date value is earlier than today
# The input is something like "2022-05-02|file.md|" and if the date part
# is earlier than today, the string "file.md" is printed
split=$(echo $1 | cut -c -10)
today=$(date +%s)
other=$(date -d $split +%s)

if [ $today -ge $other ]; then
    echo "${1::length-1}" | cut -c 12-
fi

