#!/usr/bin/bash

# This script returns a list of source Markdown files ordered
# chronologically according to the :date: field in the source
# and removing articles with date in the future
find content/posts/**/index.md -printf "%p:" -exec grep "^date:" {} \; | awk -f scripts/invert.awk | sort | sed -f scripts/filter.sed | xargs -n1 scripts/filter_if_earlier.sh
