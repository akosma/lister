# This AWK script inverts a string composed of two parts separated by a
# space, inserting a pipe at the end
{for(i=NF;i>=1;i--) printf "%s|", $i;print ""}

